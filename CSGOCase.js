var Discord = require('discord.io');
var logger = require('winston');
var fs = require('fs');

logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
var bot = new Discord.Client({
   token: 'NDg3NjI1MzUwNDgyNzU1NjA0.XfjhTg.3BxEWyTNOqJP0z_wYXW0wNF00eE',
   autorun: true
});
//set presence (0 - playing | 1 - streaming | 2 - listening to | 3 - watching)
presence = Math.floor(Math.random() * 4);
if (presence === 1) {
    presence = Math.floor(Math.random() * 4);
}
bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info(bot.username + ' - (' + bot.id + ')');
    bot.setPresence({
        game: {
            type: presence,
            name: "Need help? | !help"
        }
    });
});

bot.on('message', function (user, userID, channelID, message, evt) {
    if (message.substring(0, 1) === '!' && userID !== bot.id /* Empêche le bot de faire ses propres commandes && !botsid.includes(userID) */) {
        var args = message.substring(1).split(' ');
        var cmd = args[0];
        args = args.splice(1);

        if (bot.directMessages[channelID]) { //verifie si le channel est un dm
            estdm = true;
        }
        else{
            //Récuperer les informations d'un utilisateur
            var leUser = bot.servers[bot.channels[channelID].guild_id].members[userID];
        }

        //Chemin des fichiers 
        var path = "C:\\Users\\denis\\Desktop\\CSGOCase\\userID\\";
        var path_msg = "C:\\Users\\denis\\Desktop\\CSGOCase\\msg\\";
        var path_rank = "C:\\Users\\denis\\Desktop\\CSGOCase\\rank\\";

        //Vériifier si l'utilisateur existe
        function verifFichier(userID) {
            if (!fs.existsSync(path + userID + '.json')) {
                var obj = { data: [] };
                obj.data.push({ "Rank": "Silver I" }, { "Dollars": 0 }, { "XP": 0 }, { "XPmax": 10000 }, { "Level": 0 })
                var json = JSON.stringify(obj);
                fs.writeFileSync(path + userID + '.json', json);
            }
        }
       
        //Fonctions permettant de lire/écrire dans un fichier
        function write_userID() {
            json = JSON.stringify(Read);
            fs.writeFileSync(path + userID + '.json', json);
        }
        function read_userID() {
            Read = fs.readFileSync(path + userID + '.json', 'utf8');
            Read = JSON.parse(Read);
        }
        function write_args(ArgWrite) {
            json = JSON.stringify(Read);
            fs.writeFileSync(path + ArgWrite + '.json', json);
        }
        function read_args(ArgRead) {
            Read = fs.readFileSync(path + ArgRead + '.json', 'utf8');
            Read = JSON.parse(Read);
        }

        function CSGOLevels(){
            var existing_lvl = ['Silver I', 'Silver II', 'Silver III', 'Silver IV', 'Silver Elite', 'Silver Elite Master',
                'Gold Nova I', 'Gold Nova II', 'Gold Nova III', 'Gold Nova Master',
                'Master Guardian I', 'Master Guardian II', 'Master Guardian Elite',
                'Distinguished Master Guardian', 'Legendary Eagle', 'Legendary Eagle Master',
                'Supreme Master First Class', 'Global Elite'];
        }
    
        switch (cmd) {
            case 'help':
                verifFichier(userID);
                if (args[0] === undefined || args[0] === null) {
                    bot.deleteMessage({
                        channelID: channelID,
                        messageID: evt.d.id
                    });
                    bot.sendMessage({
                        to: userID,
                        message: "",
                        embed: {
                            color: 13172736,
                            author: {
                                name: "CSGO Case",
                                icon_url: ""
                            },
                            fields: [
                                {
                                    name: 'Commands for help',
                                    value: fs.readFileSync(path_msg + 'csgo_help' + '.txt', 'utf8')
                                }
                            ]
                        }
                    });
                    if (!bot.directMessages[channelID]) {
                        bot.sendMessage({
                            to: channelID,
                            message: ":white_check_mark: Commands sent in private"
                        });
                    }
                }
            break;

            case 'case':
                if(args[0] === null || args[0] === undefined){
                    read_userID();
                    rank = Read.data[0].Rank
                    bot.sendMessage({
                        to: userID,
                        message: "",
                        embed: {
                            color: 13172736,
                            author: {
                                name: rank + ' Case',
                                icon_url: ""
                            },
                            fields: [
                                {
                                    name: "Name | Price",
                                    value: fs.readFileSync(path_rank + rank + '.txt', 'utf8')
                                }
                            ]
                        }
                    });
                }
                else{
                    rank = args[0] + ' ' + args[1] + ' ' + args[2] + ' ' + args[3];
                        bot.sendMessage({
                            to: userID,
                            message: "",
                            embed: {
                                color: 13172736,
                                author: {
                                    name: rank + ' Case',
                                    icon_url: ""
                                },
                                fields: [
                                    {
                                        name: "Name | Price",
                                        value: fs.readFileSync(path_rank + rank + '.txt', 'utf8')
                                    }
                                ]
                            }
                        });
                }
            break;

        }
    }   
});
